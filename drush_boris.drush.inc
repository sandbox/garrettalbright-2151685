<?php

/**
 * @file
 * Boris REPL (run-eval-print loop) support for Drush.
 *
 * Boris implements an interactive shell for typing and executing PHP commands.
 * The PHP CLI application has its own which can be started by using the `-a`
 * option, but Boris is prettier and more robust - `php -a` will dump you back
 * to the shell if a fatal error occurs, but Boris will keep you in business.
 * This Drush command invokes Boris from within Drush, so Drupal functions and
 * classes (as well as Drush's) are available to you from within the shell
 * without having to go the other way around (bootstrapping Drupal from within
 * Boris) first.
 *
 * As this is merely a Drush command and not a full module, don't put it inside
 * a Drupal modules directory - it won't work. Put it inside a Drush commands
 * directory instead. See the below for possible directories.
 *
 * @see http://api.drush.org/api/drush/includes%21command.inc/function/drush_commandfile_list/6.x
 * @see drush_commandfile_list()
 */

/**
 * Implements hook_drush_command().
 */
function drush_boris_drush_command() {
  $items = array();
  $items['boris'] = array(
    'description' => 'Initialize a Boris shell with the Drupal site bootstrapped.',
    'options' => array(
      'boris-path' => array(
        'description' => 'A path to the Boris directory on your system (*not* the binary). If not given, an attempt will be made to find it automatically. If you do not have Boris in your $PATH, you may wish to set this option in a drushrc.php file to avoid having to repeatedly specify it manually; see the examples/example.drushrc.php file in the core Drush directory for more information.',
        'example_value' => '/opt/local/bin/boris/',
      ),
    ),
  );
  return $items;
}

/**
 * Validation function for our Drush command.
 */
function drush_drush_boris_boris_validate() {
  if (drush_is_windows()) {
    return drush_set_error('BORIS_WIN', dt('Boris does not currently work in Windows environments.'));
  }
  if (!function_exists('posix_getpid')) {
    return drush_set_error('BORIS_NO_POSIX', dt('The POSIX PHP library does not appear to be installed.'));
  }
  if (!function_exists('pcntl_signal')) {
    return drush_set_error('BORIS_NO_PCNTL', dt('The Process Control (pcntl) PHP library does not appear to be installed.'));
  }
  // @todo Do we also need to check for readline? That's a requirement for
  // Drush itself, isn't it?
}

/**
 * Our Drush command.
 */
function drush_drush_boris_boris() {
  // @todo Is there a way to check and set this in the validate function?
  if (!$boris_path = drush_get_option('boris-path')) {
    if (drush_shell_exec('which boris')) {
      $output = drush_shell_exec_output();
      $boris_path = dirname(realpath($output[0])) . '/..';
    }
  }
  $boris_path = realpath($boris_path);
  if (empty($boris_path)) {
    return drush_set_error('BORIS_NO_BORIS', dt('The path to the Boris directory is incorrect or could not be correctly determined.'));
  }
  $boris_autoload_path = realpath($boris_path . '/lib/autoload.php');
  if (empty($boris_autoload_path)) {
    return drush_set_error('BORIS_NO_BORIS', dt('The path to the Boris directory is incorrect or could not be correctly determined.'));
  }

  require_once $boris_autoload_path;
  $boris = new \Boris\Boris();
  // Calling this here stops "Drush command terminated abnormally" from
  // appearing (three times!) after Boris is exited.
  drush_bootstrap_finish();
  $boris->start();
}
